#!/usr/local/bin/python3

import datetime
import os
import sys
sys.path.append('/Users/tom/')

import Orgnode

# semi-hardcode the Org version, for org-protocol compatibility
orgversion = 9

todo_items = []


files = []


# We use emacsclient to retrieve a LISP list of all .org files that
# are also used by emacs for the global todo list. We immediately
# remove LISP specific formatting.
cmd = '/usr/local/bin/emacsclient -e "(org-agenda-files)"| sed "s/[()\\\"]//g"'

# Here we actually open the pipe to read from emacslient, read from it
# and split the output
for fl in os.popen(cmd).read().rstrip().split(" "):

    # For ech file we loop over the nodes
    for node in Orgnode.makelist(fl):
        if node.Todo() == 'TODO':

            if isinstance(node.Scheduled(), datetime.datetime):
            
                if node.Scheduled() > datetime.datetime.now():
                    todo_items.append( "%s (%s)" % (
                        node.Heading(), node.Scheduled() ) )
            
            elif isinstance(node.Scheduled(), datetime.date):

                if node.Scheduled() <= datetime.date.today():
                    
                    todo_items.append( "%s (%s)" % (
                        node.Heading(), node.Scheduled() ) )

            else:
                
                todo_items.append( "%s" % ( node.Heading() ) )
            


print ( "[", len(todo_items), "]" )
print ("---")



# the command
# emacsclient  -e "(org-todo-list)" -e "(select-frame-set-input-focus (selected-frame))"
# should bring up the global todo list and focus on that emacs window

if (orgversion <= 8):
    print ( ( "Global ToDo List: %d items | "
              + "bash=\"/usr/local/bin/emacsclient\""
              + "param1=\"org-protocol://todo://\" terminal=false" )
            % len(todo_items) )
else:
    print ( ( "Global ToDo List: %d items | "
              + "bash=\"/usr/local/bin/emacsclient\""
              + "param1=\"org-protocol://todo?\" terminal=false" )
            % len(todo_items) )
    
#print ( "Global ToDo List: %d items | bash=\"/usr/local/bin/emacsclient\" param1=\"org-protocol://todo://\" terminal=false" % len(todo_items) )
print ("---")

print ( "Test | bash=\"/usr/local/bin/emacsclient\" param1=\"-n\" param2=\"+15\" param3=\"/Users/tom/notes/todo.org\" terminal=false"  )
print ("---")

#print ("Current time: %s" % datetime.datetime.now())
#print ("Current date: %s" % datetime.date.today())
#print ("---")




for itm in todo_items:
    print (itm)
